package com.controller;


import com.config.FirstConfiguration;
import com.otherbeans.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class BeanController {
    private Logger logger = LogManager.getLogger(BeanController.class);
    public AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

    public void getAllBeans() {
        String[] definitionNames = context.getBeanDefinitionNames();
        for (int i = 0; i < definitionNames.length; i++) {
            logger.info(definitionNames[i]);
        }

    }

    public BeanController() {
        context.register(FirstConfiguration.class);
        context.refresh();
    }

    public void getBeanList() {
        BeanCollection beanCollection = context.getBean(BeanCollection.class);
        beanCollection.getElement();
    }

    public void getScopedBeans() {
        for (int i = 0; i < 2; i++) {
            logger.info("\nSingleton  " + context.getBean(OtherBeanA.class));
            logger.info("Prototype  " + context.getBean(OtherBeanB.class));
            logger.info("Nothing  " + context.getBean(OtherBeanC.class));
        }
    }

    public void getBeanCreator() {
        BeanCreator beanCreator = context.getBean(BeanCreator.class);
        logger.info(beanCreator.getBeanInterface1().toString());
        logger.info(beanCreator.getBeanInterface2().toString());
        logger.info(beanCreator.getBeanInterface3().toString());
    }
}
