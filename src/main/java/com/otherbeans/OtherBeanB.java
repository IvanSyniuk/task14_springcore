package com.otherbeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Scope("prototype")
public class OtherBeanB implements BeanInterface{
    private Logger logger = LogManager.getLogger(OtherBeanB.class);
    @Override
    public void doSmth() {
        logger.info(OtherBeanB.class.getName());
    }
}
