package com.otherbeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BeanCollection {
    private Logger logger = LogManager.getLogger(BeanCollection.class);
    @Autowired
    private List<BeanInterface> beanInterfaces;

    public void getElement() {
        for (int i = 0; i < beanInterfaces.size(); i++) {
            logger.info(beanInterfaces.get(i));
        }
    }
}
