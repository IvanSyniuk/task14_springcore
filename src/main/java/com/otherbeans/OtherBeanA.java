package com.otherbeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
@Primary
@Scope("singleton")
public class OtherBeanA implements BeanInterface{

    private Logger logger = LogManager.getLogger(OtherBeanA.class);
    @Override
    public void doSmth() {
        logger.info(OtherBeanA.class.getName());
    }
}
