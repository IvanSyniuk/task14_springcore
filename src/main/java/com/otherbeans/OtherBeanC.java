package com.otherbeans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class OtherBeanC implements BeanInterface{
    private Logger logger = LogManager.getLogger(OtherBeanC.class);
    @Override
    public void doSmth() {
        logger.info(OtherBeanC.class.getName());
    }
}
