package com.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanCreator {


    @Autowired
    @Qualifier("otherBeanB")
    private BeanInterface beanInterface2;
    private BeanInterface beanInterface1;
    private BeanInterface beanInterface3;


    @Autowired
    public BeanCreator(OtherBeanA otherBeanA) {
        this.beanInterface1 = otherBeanA;
    }

    @Autowired
    @Qualifier("otherBeanC")
    public void setBeanInterface3(BeanInterface beanInterface3) {
        this.beanInterface3 = beanInterface3;
    }

    public BeanInterface getBeanInterface2() {
        return beanInterface2;
    }

    public BeanInterface getBeanInterface1() {
        return beanInterface1;
    }

    public BeanInterface getBeanInterface3() {
        return beanInterface3;
    }
}
