package com.beans1;

import com.otherbeans.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanA {
    private String name;
    private int value;
    private OtherBeanA otherBeanA;

    @Autowired
    public BeanA(@Qualifier("otherBeanA") OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public OtherBeanA getOtherBeanA() {
        return otherBeanA;
    }

    public void setOtherBeanA(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
