package com.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.context.annotation.Import;

@ComponentScan(useDefaultFilters = true)
@Configuration
@ComponentScan("com.beans1")
@ComponentScan("com.otherbeans")
@Import(SecondConfiguration.class)
public class FirstConfiguration {

}
