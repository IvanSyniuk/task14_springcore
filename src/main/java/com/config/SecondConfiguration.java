package com.config;

import com.beans2.CatAnimal;
import com.beans3.BeanE;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(value = "com.beans2", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CatAnimal.class))
@ComponentScan(value = "com.beans3", excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class))
public class SecondConfiguration {
}
