package com.view;

import com.controller.BeanController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class MainView {
    private Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MainView.class);

    public void showMenu() {
        BeanController beanController = new BeanController();
        boolean flag = true;
        do {
            menu();
            switch (scanner.nextInt()) {
                case 1:
                    beanController.getBeanList();
                    break;
                case 2:
                    beanController.getScopedBeans();
                    break;
                case 3:
                    beanController.getBeanCreator();
                    break;
                case 4:
                    beanController.getAllBeans();
                    break;
                case 5:
                    flag = false;
            }
        } while (flag);


    }

    public void menu() {
        logger.info("\n1:show bean list\n2:show scoped beans\n3:show beans from BeanCreator\n4:show all beans\n5:exit");
    }
}
